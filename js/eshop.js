let cart = {}; //корзина

$('document').ready(function() {
    loadGoods();
    checkCart();
    showMiniCart();
});

function loadGoods() {
    //выгрузка товаров из json
    $.getJSON('goods.json', function(data) {
        let output = '';
        for (let key in data) {
            output += '<div class="goods__item">'
            output += '<div class="good">'
            output += '<div class="good__main">'
            output += '<img src="' + data[key].img + '" class="good__image">';
            output += '<div class="good__text">'
            output += '<h3 class="good__title">' + data[key].name + '</h3>';
            output += '<div class="good__description">' + data[key].description + '</div>';
            output += '</div>'
            output += '</div>'
            output += '<div class="good__side">'
            output += '<div class="good__cost">' + data[key].cost + ' руб.</div>';
            output += '<button data-art="' + key + '" class="button good__add">В корзину!</button>';
            output += '</div>'
            output += '</div>'
            output += '</div>'
        }
        $('.goods__main').html(output);
        $('button.good__add').on('click', addToCart);
    });
}

function addToCart() {
    //добавление товара в корзину
    let articul = $(this).attr('data-art');
    if (cart[articul] !== undefined) {
        cart[articul]++;
    } else {
        cart[articul] = 1;
    }
    localStorage.setItem('cart', JSON.stringify(cart));
    console.log(cart);
    showMiniCart();
}

function checkCart() {
    //проверка наличия товаров в корзине в localStorage
    if (localStorage.getItem('cart') != null) {
        cart = JSON.parse(localStorage.getItem('cart'));
    }
}

function showMiniCart() {
    //показать содержимое корзины
    let output = '';
    for (let key in cart) {
        output += key + ' --- ' + cart[key] + '<br>';
    }
    // $('.basket__main').html(output);
    $.getJSON('goods.json', function(data) {
        let goods = data;
        checkCart();
        showCart();

        function showCart() {
            //показать содержимое корзины
            let output = '';
            for (let key in cart) {
                output += '<div class="basket__item">';
                output += '<button class="basket__delete" data-art="' + key + '">x</button>';
                output += '<div class="basket__product">' + goods[key].name + '</div>';
                output += '<div class="basket__product-price">' + (goods[key].cost * cart[key]) + ' руб.' + '</div>';
                output += '</div>';
            }
            $('.basket__main').html(output);
            // let summ = 0;
            // $(".basket__product-price").each(function() {
            //     summ += parseInt($(this).html(), 10);
            //     $('.price__total').html(summ + ' руб.');
            // })
            totalPrise();
            $('.basket__delete').on('click', deleteGoods);
            $('.basket__submit').on('click', checkout);
        }

        function totalPrise() {
            //посчитать сумму товара в корзине
            let summ = 0;
            $(".basket__product-price").each(function() {
                summ += parseInt($(this).html(), 10);
                $('.price__total').html(summ + ' руб.');
            });
        }

        function deleteGoods() {
            //удаление товара из корзины
            let articul = $(this).attr('data-art');
            delete cart[articul];

            totalPrise();
            if (localStorage.getItem('cart') != null) {
                $('.price__total').html('0.00 руб.');
            };
            saveCartToLS(); //сохраняю корзину в localStorage
            showCart();
        }

        function checkout() {
            //Нажатие на кнопку "Оформить заказ"
            let busketList = '';
            $(".basket__product").each(function() {
                busketList += ($(this).text()) + ' ';
            });
            console.log(busketList);
            let summ = 0;
            $(".basket__product-price").each(function() {
                summ += parseInt($(this).html(), 10);
            })

            alert('Вы добавили в корзину " ' + busketList + '"на сумму ' + summ + ' руб.');
        }
    });
}

function saveCartToLS() {
    localStorage.setItem('cart', JSON.stringify(cart));
}